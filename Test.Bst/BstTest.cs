using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bst;

namespace Test.Bst
{
    [TestClass]
    public class BstTest
    {
        [TestMethod]
        public void CreateBst()
        {
            var bst = new BinarySearchTree<int>(6);
            bst.Insert(4);
            bst.Insert(9);
            bst.Insert(8);
            bst.Insert(0);
            bst.Insert(5);
            bst.Insert(1);
            bst.Insert(123);

            Assert.IsTrue(bst.Root.Value == 6);
            Assert.IsTrue(bst.Root.Right.Value == 9);
            Assert.IsTrue(bst.Root.Right.Left.Value == 8);
            Assert.IsTrue(bst.Root.Right.Right.Value == 123);

            Assert.IsTrue(bst.Root.Left.Value == 4);
            Assert.IsTrue(bst.Root.Left.Left.Value == 0);
            Assert.IsTrue(bst.Root.Left.Right.Value == 5);
            Assert.IsTrue(bst.Root.Left.Left.Right.Value == 1);
        }

        [TestMethod]
        public void CreateWithMultiple()
        {
            var bst = new BinarySearchTree<int>(6, 4, 9, 8, 0, 123);

            Assert.IsTrue(bst.Root.Value == 6);
            Assert.IsTrue(bst.Root.Left.Value == 4);
            Assert.IsTrue(bst.Root.Right.Value == 9);
            Assert.IsTrue(bst.Root.Right.Left.Value == 8);
            Assert.IsTrue(bst.Root.Right.Right.Value == 123);
            Assert.IsTrue(bst.Root.Left.Left.Value == 0);
        }

        [TestMethod]
        public void ExistsWithMultiple()
        {
            var bst = new BinarySearchTree<int>(6, 4, 9, 8, 0, 123);
            var touched = 0u;
            Assert.IsTrue(bst.Find(0, ref touched));
            Assert.IsTrue(bst.Find(123, ref touched));
            Assert.IsFalse(bst.Find(34, ref touched));
            Assert.IsFalse(bst.Find(34765, ref touched));
        }

        [TestMethod]
        public void ExistsTest()
        {
            var bst = new BinarySearchTree<int>(6);
            bst.Insert(4);
            bst.Insert(9);
            bst.Insert(8);
            bst.Insert(0);
            bst.Insert(5);
            bst.Insert(1);
            bst.Insert(123);

            var touched = 0u;

            Assert.IsTrue(bst.Find(0, ref touched));
            Assert.IsTrue(touched == 3);

            touched = 0;
            Assert.IsTrue(bst.Find(123, ref touched));
            Assert.IsTrue(touched == 3);

            touched = 0;
            Assert.IsFalse(bst.Find(34, ref touched));
            Assert.IsTrue(touched == 4);

            touched = 0;
            Assert.IsFalse(bst.Find(34765, ref touched));
            Assert.IsTrue(touched == 4);

            touched = 0;
            Assert.IsTrue(bst.Find(5, ref touched));
            Assert.IsTrue(touched == 3);

            touched = 0;
            Assert.IsTrue(bst.Find(1, ref touched));
            Assert.IsTrue(touched == 4);
        }
    }
}
