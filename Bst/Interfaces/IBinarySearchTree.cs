﻿using System;

namespace Bst.Interfaces
{
    public interface IBinarySearchTree<T> where T : IComparable<T>
    {
        void Clear();

        bool Find(T val, ref uint touched);

        void Insert(T val);
    }
}
