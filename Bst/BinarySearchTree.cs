﻿using System;
using Bst.Interfaces;

namespace Bst
{
    public class BinarySearchTree<T> : IBinarySearchTree<T> where T : IComparable<T>
    {
        public Node<T> Root { get; set; }
        
        public BinarySearchTree(T root)
        {
            Root = new Node<T>(root);
        }

        public BinarySearchTree(T root, params T[] other)
        {
            Root = new Node<T>(root);

            if(other != null && other.Length > 0)
            {
                foreach (var item in other)
                {
                    Insert(item);
                }
            }
        }

        public void Clear() {
            Root = null;
        }

        public bool Find(T val, ref uint touched)
        {
            var temp = Root;
            return Find(val, ref temp, ref touched);
        }

      
        public void Insert(T val)
        {
            var temp = Root;
            Insert(val, ref temp);
        }

        public Node<T> GetSubtree(T val, ref uint touched)
        {
            var temp = Root;
            return GetSubtree(val, ref temp, ref touched);
        }

        #region private handlers
        private bool Find(T val, ref Node<T> parent, ref uint touched)
        { 
            touched++;

            if (parent == null)
            {
                return false;
            }

            if (parent.Value.CompareTo(val) == 0)
            {
                return true;
            }

            Node<T> temp = null;

            if (parent.Value.CompareTo(val) > 0)
            {
                temp = parent.Left;
            }
            else if (parent.Value.CompareTo(val) < 0)
            {
                temp = parent.Right;
            }

            return Find(val, ref temp, ref touched);
        }

        private Node<T> GetSubtree(T val, ref Node<T> parent, ref uint touched)
        {
            touched++;

            if (parent == null)
            {
                return null;
            }

            if (parent.Value.CompareTo(val) == 0)
            {
                return parent;
            }

            Node<T> res = null;

            if (parent.Value.CompareTo(val) > 0)
            {
                res = parent.Left;
            }
            else if (parent.Value.CompareTo(val) < 0)
            {
                res = parent.Right;
            }

            return GetSubtree(val, ref res, ref touched);
        }


        private void Insert(T val, ref Node<T> parent)
        {
            if(parent == null)
            {
                parent = new Node<T>(val);
                return;
            }

            if(parent.Value == null)
            {
                parent.Value = val;
                return;
            }

            Node<T> temp = null;

            if(val.CompareTo(parent.Value) < 0)
            {
                temp = parent.Left;
                Insert(val, ref temp);
                parent.Left = temp;
            }
            else if(val.CompareTo(parent.Value) > 0)
            {
                temp = parent.Right;
                Insert(val, ref temp);
                parent.Right = temp;
            }
        }
        #endregion
    }
}
