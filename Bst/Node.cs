﻿namespace Bst
{
    public class Node<T>
    {
        private BinaryNodeList<T> _children;

        public T Value { get; set; }

        public Node<T> Left {
            get {
                return _children[0];
            }
            set {
                _children[0] = value;
            }
        }

        public Node<T> Right {
            get {
                return _children[1];
            }
            set {
                _children[1] = value;
            }
        }

        public Node(T value)
        {
            Value = value;
            _children = new BinaryNodeList<T>();
        }
    }
}
